## Vita First Sight
- Love at First Sight "port" for the PS Vita.
- Requires a desktop copy of the game.

### Warnings
- Loading the game will take a bit of time.
- To open the pause menu while playing, press START.
- You should be able to access all buttons with the D-Pad (but if you can't, use the touchscreen).

### Instructions
0. Download the VPK from the releases section of this repo.
1. Install the VPK on your Vita.
2. Extract the vpk on desktop (change .vpk extension to .zip first if needed).
3. Locate the installation folder of the game on your computer.
4. Create a copy of the `game` folder in the same location as the extracted vpk's folder.
5. Use one of the patches in the vpk folder's `patches` folder:

  * For [bsdiff](https://www.daemonology.net/bsdiff/) / Linux:
    - Install `bsdiff` through your distro's package manager.
    - Copy `bsdiff.rpa` into the copy of the `game` folder you made in step 4.
    - Run `bspatch archive.rpa archive.rpa.new bsdiff.rpa` .

  * For [xdelta3](http://xdelta.org/) / Windows:
    - Install / download a precompiled binary (.exe) for `xdelta3`.
    - Download the xdelta patch using the URL in `patches\xdelta3.url`
    - Copy `xdelta3.rpa` into the copy of the `game` folder you made in step 4.
    - Make sure that your `xdelta.exe` is in a folder that's a part of your `PATH` variable **OR**
      - copy `xdelta.exe` into the `game` folder.
    - Open up a cmd prompt or powershell window in that folder.
    - Run `xdelta3.exe -d -s archive.rpa xdelta3.rpa archive.rpa.new` .

6. Delete the original `archive.rpa` and rename `archive.rpa.new` to `archive.rpa`.
7. Now copy that `game` folder to `ux0:app/RLYX00004/` on your Vita.

### Credits
- Made possible by SonicMastr's [Ren'Py Vita Project](https://github.com/SonicMastr/renpy-vita).
